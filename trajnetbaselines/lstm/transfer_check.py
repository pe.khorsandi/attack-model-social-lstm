"""Command line tool to train an LSTM model."""

# todo: clean the input section (remove unnecessary args and add new ones)

from datetime import datetime
import pdb
import argparse

import logging
import socket
import math
import sys
import copy
import time
import random
import os
import pickle
import torch
import numpy as np
from tqdm import tqdm

from torch.autograd import Variable
from torch import nn
from operator import itemgetter, attrgetter
from threading import Thread
import trajnetplusplustools

from .lstm import LSTM, drop_distant
from .gridbased_pooling import GridBasedPooling
from .non_gridbased_pooling import NN_Pooling, HiddenStateMLPPooling, AttentionMLPPooling, DirectionalMLPPooling
from .non_gridbased_pooling import NN_LSTM, TrajectronPooling, SAttention, SAttention_fast
from .more_non_gridbased_pooling import NMMP

from .. import __version__ as VERSION

from .utils import center_scene, random_rotation, save_log, calc_fde_ade, save_tensor_to_csv, append_list_as_row
from .utils import paths, paths_one, erase_log

def save_model(epoch, model, loss, PATH, model_name):
    torch.save(model.state_dict(), PATH + model_name + '.epoch.' + str(epoch))


def pointwise_perturbation(output, ground_truth):  # input: two tensors, returns fde, ade
    l = output.tolist()
    l2 = ground_truth.tolist()
    num_frames_output = len(l)
    num_frames_truth = len(l2)
    delta = num_frames_output - num_frames_truth
    distances = []
    for frame in range(num_frames_output):
        if frame + num_frames_truth >= num_frames_output:
            x1 = l[frame][0][0]  # for agent 0
            y1 = l[frame][0][1]
            x2 = l2[frame - delta][0][0]
            y2 = l2[frame - delta][0][1]
            d = np.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
            distances.append(d)

    return distances

def draw_one_tensor(filename, real):
    real = real.permute(1, 0, 2).tolist()
    with paths_one(real, filename):
        pass

def draw_two_tensor(filename, real, perturb, collision_point_neighbor=None, collision_point_main=None):
    real = real.permute(1, 0, 2).tolist()
    perturb = perturb.permute(1, 0, 2).tolist()
    with paths(perturb, real, filename, collision_point_neighbor, collision_point_main):
        pass

class Trainer(object):
    def __init__(self, model=None, criterion='L2', lr=None, barrier=1, show_limit=30,
                 device=None, batch_size=32, obs_length=9, pred_length=12, augment=False,
                 normalize_scene=False, save_every=1, start_length=0, obs_dropout=False,
                 sample_size = 70,
                 reg_noise=0.5, reg_w=1, pre_trained=False, attacker_model_name='attacker.state',
                 discriminator_model_name='discriminator.state', collision_type = 'tanh',
                 perturb_all = 'true', threads_limit = 4, speed_up='false', saving_name=""):

        now = datetime.now()
        date_time = now.strftime("%m.%d.%Y, %H:%M:%S")

        # Counters to find the collision ratio
        self.collision_counter = 0
        self.fail_counter = 0
        self.collision_type = collision_type

        self.model = model if model is not None else LSTM()

        self.device = device if device is not None else torch.device('cpu')

        self.model = self.model.to(self.device)

        # Learning Parameters
        self.lr = lr
        self.barrier = barrier
        self.batch_size = batch_size
        self.reg_noise = reg_noise
        self.reg_w = reg_w
        self.sample_size = sample_size
        self.perturb_all = perturb_all
        self.threads = []
        self.threads_limit = threads_limit
        self.models = [copy.deepcopy(self.model) for _ in range(self.threads_limit)]
        self.speed_up = speed_up
        
        self.show_limit = show_limit  # number of samples to draw
        
        #Address Stuff
        self.base_dir = 'trajnetplusplustools/scenes/' + saving_name + '/'
        self.saving_name = saving_name
        self.sample_status_address = "Sample Status-" + self.saving_name +".txt"
        if not os.path.exists(self.base_dir):
          os.makedirs(self.base_dir)


        self.log = logging.getLogger(self.__class__.__name__)
        self.save_every = save_every

        # Observation and Prediction length
        self.obs_length = obs_length
        self.pred_length = pred_length
        self.seq_length = self.obs_length + self.pred_length

        self.augment = augment
        self.normalize_scene = normalize_scene

        self.start_length = start_length
        self.obs_dropout = obs_dropout

        # ADE FDE Records
        self.all_fde = {}
        self.all_ade = {}
        self.all_fde['observed'] = []
        self.all_ade['observed'] = []
        self.all_fde['perturb'] = []
        self.all_ade['perturb'] = []
        self.all_fde['delta'] = []
        self.all_ade['delta'] = []
        self.results_log = ''
        self.count_draw = 0
        self.save_perturbed_data_groundtruth = []
        self.save_perturbed_data_modelprediction = []
        self.save_real_data = []


    def add_to_log(self, new_line):
        print(new_line)
        self.results_log = self.results_log + new_line + '\n'

    def numerical_stats(self):  # prints numerical stats

        # ___________________
        self.add_to_log("FDE on observed trajectories of observations in the test set")
        self.add_to_log(str(round(np.mean(self.all_fde['observed']), 4)))

        self.add_to_log("ADE on observed trajectories of observations in the test set")
        self.add_to_log(str(round(np.mean(self.all_ade['observed']), 4)))
        # ___________________
        

    def loop(self, train_scenes, val_scenes, train_goals, val_goals, out):
        models_path = 'trajnetbaselines/lstm/Attack-Models'
        self.train(train_scenes, train_goals, 1)

    def outputfile_checkpoint(self, ted):
        text_file = open("numerical_log_collision_" + self.saving_name + ".txt", "w")
        self.add_to_log("\n--------------------END OF CHECKPOINT(" + ted + "--------------------\n")
        all_ade_fde = self.results_log
        text_file.write(all_ade_fde)
        text_file.close()

        dataset = {}
        
        dataset['real'] = self.save_real_data
        dataset['perturb_g'] = self.save_perturbed_data_groundtruth
        dataset['perturb_p'] = self.save_perturbed_data_modelprediction 

        dataset_file = open('trajnetbaselines/lstm/Data/aug_data' + self.saving_name + '.pkl', 'ab')
        pickle.dump(dataset, dataset_file)
        dataset_file.close()
        print("saved collided data till now: ", len(self.save_perturbed_data_modelprediction ))


    def train(self, scenes, goals, epoch):
        erase_log(self.sample_status_address)
        dataset_file = open('trajnetbaselines/lstm/Data/aug_data.pkl', 'rb')
        dataset = pickle.load(dataset_file)
        for scene_i, x  in enumerate(dataset['perturb_g']):
            scene = x[0]
            scene_goal = x[1]
            rel = dataset['real'][scene_i]
            self.train_batch(scene, scene_goal, scene_i, rel[0], rel[1])
        save_log("Collision Ratio: " + str(self.collision_counter/(self.collision_counter + self.fail_counter)), self.sample_status_address)
        print("Collision Ratio: " + str(self.collision_counter/(self.collision_counter + self.fail_counter)), self.sample_status_address)
    def random_x(self, x):
      y = 5
      mod = 1000000007
      for i in range(y):
        x = ((113 * x) + 81) % mod
      return x


    def clamp(self, vector, barrier):
        n = torch.norm(vector.view(-1, 2), 2, dim=1)
        for i in range(len(n)):
          if n[i] > barrier:
            vector[2 * i] *= barrier / n[i]
            vector[2 * i + 1] *= barrier/ n[i]
        return vector

    
    def train_batch(self, xy, goals, scene_id, xy2, goals2):
        
        local_model = self.model
        collision_done_barrier = 0.3
        # xy, perturb,          xy2, real
        observed = xy[self.start_length:self.obs_length].clone()
        observed2 = xy2[self.start_length:self.obs_length].clone()
        first_observed = xy[self.start_length:self.obs_length].clone()
        first_observed2 = xy2[self.start_length:self.obs_length].clone()

        fde, ade = calc_fde_ade(output=observed2, ground_truth=observed)
        self.all_ade['observed'].append(ade)
        self.all_fde['observed'].append(fde)


        
        chert, outputs= local_model(observed2, goals2.clone(), n_predict = 12)
        chert, outputs_perturbed = local_model(observed, goals.clone(), n_predict = 12)
        
        copy_outputs_perturbed = outputs_perturbed.clone()
        outputs_perturbed_nan = torch.isnan(outputs_perturbed)

        for i in range(len(outputs_perturbed)):
            for j in range(len(outputs_perturbed[0])):
                if any(outputs_perturbed_nan[i, j].tolist()):
                    outputs_perturbed.data[i, j] = 10000
                    outputs_perturbed[i, j].detach()
        
        agents_count = len(observed[0])
        distances = torch.sqrt(torch.sum((torch.square(outputs_perturbed[-self.pred_length:]
                                  - outputs_perturbed[-self.pred_length:, 0].repeat_interleave(agents_count, 0).reshape(
                                  self.pred_length, agents_count, 2))[:, 1:]), dim=2))
        # Best Frame and Neighbor
        frame_index = (torch.argmin(distances) // (agents_count - 1)).data - self.pred_length
        neighbor_index =  (torch.argmin(distances) % (agents_count - 1) + 1).data
        best_frame = frame_index
        best_neighbor = neighbor_index 
        # Score
        best_score_by_now = torch.min(distances).data
        perturbed_observation = observed
        did_collide = False
        #print("IM here")
        if best_score_by_now < collision_done_barrier:
            self.collision_counter += 1
            #print(":D")
            did_collide = True
            #print("Collsion :D")
        else:
            self.fail_counter += 1
            #print(": ((((")
            #print("Fail : (")
        
        # Draw Images & Save Logs
        num_agents = agents_count
        best_outputs_perturbed = copy_outputs_perturbed
        if self.count_draw < self.show_limit and num_agents >= 2:
            self.count_draw += 1

            if best_score_by_now < collision_done_barrier:
                save_log("Sample " + str(self.count_draw) + " Collided.", self.sample_status_address)
            else:
                save_log("Sample " + str(self.count_draw) + " Failed.", self.sample_status_address)

            real = torch.cat((first_observed[: self.obs_length], outputs[-self.pred_length:]))
            perturb = torch.cat((first_observed2[: self.obs_length], best_outputs_perturbed[-self.pred_length:]))

            filename = self.base_dir  + str(scene_id) + '_altered_scene_ade: ' + str( round(ade, 3) )+ '.png'
            
            #filename_original = self.base_dir + str(scene_id) + '_original_scene.png'
            
            draw_two_tensor(filename, real, perturb, best_outputs_perturbed[best_frame, 0].tolist()
                , best_outputs_perturbed[best_frame, best_neighbor].tolist())
            
        
        #pdb.set_trace()
        return 0, 0, 0

def prepare_data(path, subset='/train/', sample=1.0, goals=True):
    """ Prepares the train/val scenes and corresponding goals """

    ## read goal files
    all_goals = {}
    all_scenes = []

    ## List file names
    files = [f.split('.')[-2] for f in os.listdir(path + subset) if f.endswith('.ndjson')]
    ## Iterate over file names
    for file in files:
        reader = trajnetplusplustools.Reader(path + subset + file + '.ndjson', scene_type='paths')
        ## Necessary modification of train scene to add filename
        scene = [(file, s_id, s) for s_id, s in reader.scenes(sample=sample)]
        if goals:
            goal_dict = pickle.load(open('dest_new/' + subset + file + '.pkl', "rb"))
            ## Get goals corresponding to train scene
            all_goals[file] = {s_id: [goal_dict[path[0].pedestrian] for path in s] for _, s_id, s in scene}
        all_scenes += scene

    if goals:
        return all_scenes, all_goals
    return all_scenes, None


def main(epochs=10):
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=epochs, type=int,
                        help='number of epochs')
    parser.add_argument('--barrier', default=1, type=float,
                        help='barrier for noise')
    parser.add_argument('--show_limit', default=5, type=int,
                        help='number of shown samples')
    parser.add_argument('--step_size', default=15, type=int,
                        help='step_size of scheduler')
    parser.add_argument('--save_every', default=1, type=int,
                        help='frequency of saving model')
    parser.add_argument('--obs_length', default=9, type=int,
                        help='observation length')
    parser.add_argument('--pred_length', default=12, type=int,
                        help='prediction length')
    parser.add_argument('--batch_size', default=1, type=int,
                        help='number of epochs')
    parser.add_argument('--lr', default=1e-3, type=float,
                        help='initial learning rate')
    parser.add_argument('--type', default='social',
                        choices=('vanilla', 'occupancy', 'directional', 'social', 'hiddenstatemlp', 's_att_fast',
                                 'directionalmlp', 'nn', 'attentionmlp', 'nn_lstm', 'traj_pool', 's_att', 'nn_tag',
                                 'nmmp'),
                        help='type of LSTM to train')
    parser.add_argument('--collision_type', default='social',
                        choices=('attention', 'pgd', 'tanh', 'double_w'),
                        help='Method used for collision')
    parser.add_argument('--data_part', default='test',
                        choices=('test', 'train', 'val'),
                        help='data part to perform attack on')


    parser.add_argument('--loss_type', default='L2',
                        choices=('L2', 'collision'),
                        help='type of LSTM to train')
    parser.add_argument('--norm_pool', action='store_true',
                        help='normalize_pool (along direction of movement)')
    parser.add_argument('--front', action='store_true',
                        help='Front pooling (only consider pedestrian in front along direction of movement)')
    parser.add_argument('-o', '--output', default=None,
                        help='output file')
    parser.add_argument('--disable-cuda', action='store_true',
                        help='disable CUDA')
    parser.add_argument('--augment', action='store_true',
                        help='augment scenes')
    parser.add_argument('--normalize_scene', action='store_true',
                        help='augment scenes')
    parser.add_argument('--path', default='trajdata',
                        help='glob expression for data files')
    parser.add_argument('--goal_path', default=None,
                        help='glob expression for goal files')
    parser.add_argument('--loss', default='L2',
                        help='loss function')
    parser.add_argument('--goals', action='store_true',
                        help='to use goals')
    parser.add_argument('--reg_noise', default=0.5, type=float,
                        help='noise regulizer weigth')
    parser.add_argument('--reg_w', default=1, type=float,
                        help='w regulizer weigth')
    parser.add_argument('--sample_size', default=70, type=int,
                        help='number of checked samples')
    parser.add_argument('--threads_limit', default=1, type=int,
                        help='number of checked samples')
    parser.add_argument('--perturb_all', default='true',
                        choices=('true', 'false'),
                        help='perturb all the nodes or only ones in the middle')
    parser.add_argument('--speed_up', default='false',
                        choices=('true', 'false'),
                        help='speed up?')

    pretrain = parser.add_argument_group('pretraining')
    pretrain.add_argument('--load-state', default=None,
                          help='load a pickled model state dictionary before training')
    pretrain.add_argument('--load-full-state', default=None,
                          help='load a pickled full state dictionary before training')
    pretrain.add_argument('--nonstrict-load-state', default=None,
                          help='load a pickled state dictionary before training')

    ##Pretrain Pooling AE
    pretrain.add_argument('--load_pretrained_pool_path', default=None,
                          help='load a pickled model state dictionary of pool AE before training')
    pretrain.add_argument('--pretrained_pool_arch', default='onelayer',
                          help='architecture of pool representation')
    pretrain.add_argument('--downscale', type=int, default=4,
                          help='downscale factor of pooling grid')
    pretrain.add_argument('--finetune', type=int, default=0,
                          help='finetune factor of pretrained model')

    hyperparameters = parser.add_argument_group('hyperparameters')
    hyperparameters.add_argument('--hidden-dim', type=int, default=128,
                                 help='RNN hidden dimension')
    hyperparameters.add_argument('--coordinate-embedding-dim', type=int, default=64,
                                 help='coordinate embedding dimension')
    hyperparameters.add_argument('--cell_side', type=float, default=0.6,
                                 help='cell size of real world')
    hyperparameters.add_argument('--n', type=int, default=16,
                                 help='number of cells per side')
    hyperparameters.add_argument('--layer_dims', type=int, nargs='*',
                                 help='interaction module layer dims for gridbased pooling')
    hyperparameters.add_argument('--pool_dim', type=int, default=256,
                                 help='pooling dimension')
    hyperparameters.add_argument('--embedding_arch', default='two_layer',
                                 help='interaction arch')
    hyperparameters.add_argument('--goal_dim', type=int, default=64,
                                 help='goal dimension')
    hyperparameters.add_argument('--spatial_dim', type=int, default=32,
                                 help='attention mlp spatial dimension')
    hyperparameters.add_argument('--vel_dim', type=int, default=32,
                                 help='attention mlp vel dimension')
    hyperparameters.add_argument('--pool_constant', default=0, type=int,
                                 help='background of pooling grid')
    hyperparameters.add_argument('--sample', default=1.0, type=float,
                                 help='sample ratio of train/val scenes')
    hyperparameters.add_argument('--norm', default=0, type=int,
                                 help='normalization scheme for grid-based')
    hyperparameters.add_argument('--no_vel', action='store_true',
                                 help='dont consider velocity in nn')
    hyperparameters.add_argument('--neigh', default=4, type=int,
                                 help='neighbours to consider in DirectConcat')
    hyperparameters.add_argument('--mp_iters', default=5, type=int,
                                 help='message passing iters in NMMP')
    hyperparameters.add_argument('--start_length', default=0, type=int,
                                 help='prediction length')
    hyperparameters.add_argument('--obs_dropout', action='store_true',
                                 help='obs length dropout')
    args = parser.parse_args()

    # todo: Add Items to Args
    discriminator_type = 1

    if args.sample < 1.0:
        torch.manual_seed("080819")
        random.seed(1)

    if not os.path.exists('OUTPUT_BLOCK/{}'.format(args.path)):
        os.makedirs('OUTPUT_BLOCK/{}'.format(args.path))
    if args.goals:
        args.output = 'OUTPUT_BLOCK/{}/lstm_goals_{}_{}.pkl'.format(args.path, args.type, args.output)
    else:
        args.output = 'OUTPUT_BLOCK/{}/lstm_{}_{}.pkl'.format(args.path, args.type, args.output)

    # configure logging
    from pythonjsonlogger import jsonlogger
    print(args.load_full_state)
    if args.load_full_state:
        file_handler = logging.FileHandler(args.output + '.log', mode='a')
    else:
        file_handler = logging.FileHandler(args.output + '.log', mode='w')
    file_handler.setFormatter(jsonlogger.JsonFormatter('(message) (levelname) (name) (asctime)'))
    stdout_handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, handlers=[stdout_handler, file_handler])
    logging.info({
        'type': 'process',
        'argv': sys.argv,
        'args': vars(args),
        'version': VERSION,
        'hostname': socket.gethostname(),
    })


    # refactor args for --load-state
    args.load_state_strict = True
    if args.nonstrict_load_state:
        args.load_state = args.nonstrict_load_state
        args.load_state_strict = False
    if args.load_full_state:
        args.load_state = args.load_full_state

    # add args.device
    args.device = torch.device('cpu')
    # if not args.disable_cuda and torch.cuda.is_available():
    #     args.device = torch.device('cuda')

    test_path = 'DATA_BLOCK/collision_test'
    args.path = 'DATA_BLOCK/' + args.path
    if args.data_part == 'test':
      test_scenes, test_goals = prepare_data(test_path, subset='/test_private/', sample=args.sample, goals=args.goals)
    else:
      test_scenes, test_goals = prepare_data(args.path, subset='/train/', sample=args.sample, goals=args.goals)
    
    val_scenes, val_goals = prepare_data(args.path, subset='/val/', sample=args.sample, goals=args.goals)

    ## pretrained pool model (if any)
    pretrained_pool = None

    # create model (Various interaction/pooling modules)
    pool = None
    if args.type == 'hiddenstatemlp':
        pool = HiddenStateMLPPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim,
                                     mlp_dim_vel=args.vel_dim)
    elif args.type == 'nmmp':
        pool = NMMP(hidden_dim=args.hidden_dim, out_dim=args.pool_dim, k=args.mp_iters)
    elif args.type == 'attentionmlp':
        pool = AttentionMLPPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim,
                                   mlp_dim_spatial=args.spatial_dim, mlp_dim_vel=args.vel_dim)
    elif args.type == 'directional':
        pool = DirectionalMLPPooling(out_dim=args.pool_dim)
    elif args.type == 'nn':
        pool = NN_Pooling(n=args.neigh, out_dim=args.pool_dim, no_vel=args.no_vel)
    elif args.type == 'nn_lstm':
        pool = NN_LSTM(n=args.neigh, hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 'traj_pool':
        pool = TrajectronPooling(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 's_att':
        pool = SAttention(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type == 's_att_fast':
        pool = SAttention_fast(hidden_dim=args.hidden_dim, out_dim=args.pool_dim)
    elif args.type != 'vanilla':
        pool = GridBasedPooling(type_=args.type, hidden_dim=args.hidden_dim,
                                cell_side=args.cell_side, n=args.n, front=args.front,
                                out_dim=args.pool_dim, embedding_arch=args.embedding_arch,
                                constant=args.pool_constant, pretrained_pool_encoder=pretrained_pool,
                                norm=args.norm, layer_dims=args.layer_dims)

    model = LSTM(pool=pool,
                 embedding_dim=args.coordinate_embedding_dim,
                 hidden_dim=args.hidden_dim,
                 goal_flag=args.goals,
                 goal_dim=args.goal_dim)

    # train
    # loads here ->
    load_address = 'trajnetbaselines/lstm/Target-Model/'
    all_models_name = os.listdir(load_address)
    this_model = "bad"
    for x in all_models_name:
      if x.count(args.type) == 1 and x.count(".state") == 1:
        this_model = x
    if this_model == "bad":
      print("Loding model failed")
      assert(0)
    
    load_address = load_address + this_model

    # load pretrained model.
    # useful for transfer learning
    print("Loading Model Dict")
    with open(load_address, 'rb') as f:
        checkpoint = torch.load(f)
    pretrained_state_dict = checkpoint['state_dict']
    print("Successfully Loaded")
    
    model.load_state_dict(pretrained_state_dict, strict=False)

    # Freeze the model
    for p in model.parameters():
        p.requires_grad = False

    # trainer
    # todo: add new args


    saving_name = str(args.type) + "-" + str(args.collision_type) + "-noise-" + str(args.reg_noise) + "-w-" + str(args.reg_w)
    
    
    
    trainer = Trainer(model, lr=args.lr, device=args.device, barrier=args.barrier, show_limit=args.show_limit,
                      criterion=args.loss, batch_size=args.batch_size, collision_type = args.collision_type,
                      obs_length=args.obs_length, reg_noise = args.reg_noise, reg_w = args.reg_w, 
                      pred_length=args.pred_length, augment=args.augment, normalize_scene=args.normalize_scene,
                      save_every=args.save_every, start_length=args.start_length, obs_dropout=args.obs_dropout,
                      sample_size = args.sample_size, perturb_all = args.perturb_all, threads_limit=args.threads_limit, 
                      speed_up=args.speed_up, saving_name=saving_name)
    trainer.loop(test_scenes, val_scenes, test_goals, val_goals, args.output)
    trainer.numerical_stats()


if __name__ == '__main__':
    main()
