taskset --cpu-list 2-3 python -m trajnetbaselines.lstm.adversarial_training_robust \
--augment --type nn_lstm --epochs 1  --lr 0.00003 --step_size 10 --n 16  --output one_12_6 \
--cell_side 0.6 --embedding_arch conv_two_layer --layer_dims 1024 --batch_size 8 --loss 'pred' \
--rob_type gtruth --max_pert 0.3  --min_pert -0.0001 --perturbed_percent 100 \
--load_adr '/data/saeed-data/attack-model-social-lstm/adv_training_lr1e-4_p100_gtruth_per0.3/OUTPUT_BLOCK/trajdata/aug_data_tmp.pkl' \
--models_path '/data/saeed-data/attack-model-social-lstm/adv_training_lr1e-4_p100_gtruth_per0.3/OUTPUT_BLOCK/trajdata/nn_lstm_bu.state' \
--output '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/' \

name='hoohoo'
for i in {0..2000}
do
  echo $i
  taskset --cpu-list 2-3 python -m trajnetbaselines.lstm.adversarial_training_attack \
  --augment --lr 0.01 --layer_dims 1024 --barrier 0.2 --show_limit 50 \
  --type nn_lstm --reg_noise 0.1 --reg_w 0.7 --perturb_all true --threads_limit 1 \
  --data_part train --collision_type hard --speed_up true --sample_size 5000 --enable_thread false \
  --models_path '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/lstm_nn_lstm_'$name'.pkl.epoch0.state' \
  --output '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/' \
  --samples_path '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/aug_data_tmp.pkl' \
  --batch_size 20 --which_batch $i \

  taskset --cpu-list 2-3 python -m trajnetbaselines.lstm.adversarial_training_robust \
  --augment --type nn_lstm --epochs 1  --lr 0.00003 --step_size 10 --n 16  --output one_12_6 \
  --cell_side 0.6 --embedding_arch conv_two_layer --layer_dims 1024 --batch_size 8 --loss 'pred' \
  --rob_type gtruth --max_pert 0.03  --min_pert -0.0001 --perturbed_percent 100 \
  --load_adr '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/aug_data_tmp.pkl' \
  --output '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/' \
  --which_batch $i \
  --load-full-state '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/lstm_nn_lstm_'$name'.pkl.epoch0.state' \

  if ! ((i % 200));
  then
    cp '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/lstm_nn_lstm_'$name'.pkl.epoch0.state' \
    '/data/saeed-data/attack-model-social-lstm/adv_training_lr3e-5_p100_gtruth_per0.03/OUTPUT_BLOCK/trajdata/lstm_nn_lstm_'$name'.pkl_iter'$i'.state'
  fi
done
