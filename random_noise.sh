python -m trajnetbaselines.lstm.random_noise \
--lr 0.1 \
--layer_dims 1024 \
--barrier 0.2 \
--show_limit 50 \
--sample_size 2000 \
--perturb_all true \
--threads_limit 2 \
--type nn_lstm \
--enable_thread fasle\