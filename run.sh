taskset --cpu-list 0-1 python -m trajnetbaselines.lstm.run \
--augment \
--lr 0.01 \
--layer_dims 1024 \
--barrier 0.2 \
--show_limit 50 \
--type nn_lstm \
--reg_noise 0.1 \
--reg_w 0.7 \
--perturb_all true \
--threads_limit 1 \
--data_part test \
--collision_type hard \
--speed_up true \
--sample_size 200 \
--enable_thread false \
--models_path 'trajnetbaselines/lstm/Target-Model/nn_lstm.state' \
--output 'tmp/' \
--samples_path 'tmp/aug_data_tmp.pkl'\
#--models_path '/data/saeed-data/attack-model-social-lstm/adv_training_lr1e-4_p100_gtruth_per0.3/OUTPUT_BLOCK/trajdata/lstm_nn_lstm.pkl.epoch0.state'
