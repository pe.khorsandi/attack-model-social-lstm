python -m trajnetbaselines.lstm.transfer_check \
--lr 0.1 \
--layer_dims 1024 \
--barrier 0.08 \
--show_limit 50 \
--type s_att_fast \
--collision_type attention \
--reg_noise 0.7 \
--reg_w 0.5 \
--sample_size 50 \
--perturb_all true \
--threads_limit 2 \
--speed_up true \
--data_part train \