"""visualize gan"""
import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument('--epochs', default=10, type=int,
                        help='number of epochs')
parser.add_argument('--samples', default=5, type=int,
                        help='number of samples to draw')
args = parser.parse_args()
num_epochs = args.epochs
num_samples = args.samples
for e in range(num_epochs):
    print("drawing for epoch", e, "...")
    for sample in range(num_samples):
        print("sample", sample, "/", num_samples)
        epoch = "epoch" + str(e) + '/'
        command = 'python -m trajnetplusplustools.trajectories \\\n--real scenes/'+ epoch + 'outputs' + str(sample) + '.ndjson \\'  + '\n--perturbed scenes/' + epoch + 'outputs_perturbed' + str(sample) + '.ndjson'
        #print(command)
        #ssert(0)
        os.system(command)

        #os.remove('scenes'+ '/outputs' + str(sample) + '.ndjson')
        #os.remove('scenes' + '/outputs_perturbed' + str(sample) + '.ndjson')