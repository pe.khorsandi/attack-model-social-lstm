"""visualize pgd"""
import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument('--samples', default=30, type=int,
                        help='number of samples to draw')
args = parser.parse_args()
num_samples = args.samples

for sample in range(num_samples):
    print("sample", sample, "/", num_samples)
    epoch = ""
    command = 'python -m trajnetplusplustools.trajectories \\\n--real scenes/'+ epoch + 'outputs' + str(sample) + '.ndjson \\'  + '\n--perturbed scenes/' + epoch + 'outputs_perturbed' + str(sample) + '.ndjson'
    os.system(command)