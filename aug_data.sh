# taskset --cpu-list 8-11
#data part is train because we don't want adversarial examples from test data!
python -m trajnetbaselines.lstm.run \
--lr 0.1 \
--layer_dims 1024 \
--barrier 0.2 \
--show_limit 50 \
--type nn_lstm \
--reg_noise 0.3 \
--reg_w 0.7 \
--perturb_all true \
--threads_limit 1 \
--data_part train \
--collision_type tanh \
--speed_up true \
--sample_size 2000 \
--enable_thread false\